#--------------Listas--------------------
lista=[1,2,3,4]
lista2=[1,2.33,True,"hey"] # admite tipos mixtos
lista3=[1,2,3,[4,5,6],7,8,9] # es posible tener una lista de listas
lista4 = lista5=[1,2,3,4,5]

lista2[2] = False # cambiar un valor por indice

print(lista)
print(lista2)
print(lista3)
print(lista4)
print(lista5)

# Métodos
lista.append("tec") #apilar
print(lista)
lista.insert(1, 16) #insertar(posición,valor)
print(lista)
del lista[2] #elimina un elemento en la posicion indicada
print(lista)
lista.remove(3)#elimina el elemento coincidente con el valor insetado
print(lista)
lista.pop()#elimina el ultimo elemento de la lista
print(lista)
lista.reverse()#invierte el orden
print(lista)
lista.count(1)#cueta la concurrencia de un valor
print(lista)
lista.extend(range(3))#agrega valores a una lista apartir de elementos iterables
print(lista)
lista.index(4)#indice de valores
print(lista)
lista.sort()#ordena elementos de me menor a mayor
print(lista)
lista.clear() #vaciar lista
lista1=list(range(10))#Crea una lista apartir de 0 a 9
print(lista)
print(lista1)

# Compresión

# Como regularmente lo hariamos
squares = []
for x in range(10):
	squares.append(x**2)

# con compresión
squares = [x**2 for x in range(10)]

# Encontrando valores
print(f"¿El 9 exite en la lista?\n {9 in squares}")

#------------------------------------------------

#-----------------Tuplas------------------------
ip_address = ('10.20.30.40', 8080)
# Descomenta para ver los errores
# ip_address.insert(0, '10.20.30.50')
# del ip_address[1]
# ip_address[1] = 80

#-----------------------------------------------

#------------------Diccionarios-----------------

state_capitals = {
'Arkansas': 'Little Rock',
'Colorado': 'Denver',
'California': 'Sacramento',
'Georgia': 'Atlanta'
}

print(state_capitals['Arkansas']) #accediendo a al valor de una llave

print(state_capitals.get('Arkansas')) # una mejor manera de acceder a una llave la linea anterior de no exitir la lleve mandaria a una excepción por otro lado esta linea de no exitir la llave el valor seria "None"

print(state_capitals.get('DC', 'Washington'))#El segundo parametro indica el valor que tomara de no exitir la llave

# Agregar un nuevo elemento
state_capitals["DC"] = "Washington"

# Unir o actualizar elementos < python 3.9
state_capitals.update({"Texas": "Austin"})

# Unit o actualizar elementos > python 3.9
state_capitals | {"Texas": "Austin"}

print(state_capitals.keys()) # listar todas la llaves disponibles
print(state_capitals.values()) # listar todos los valores disponibles
print(state_capitals.items()) # Comvetir a filas un diccionario [(clave, valor),...]
#-----------------------------------------------

#-------------Conjuntos------------------------
conjunto = {1, 2, 3, 4}
conjunto2 = {2, 4, 6, 8}

# Agregar
conjunto.add(5)
conjunto.update({6, 7}) # Acepta cualquier iterable

# Eliminar
conjunto.remove(7)

# Union
print(conjunto | conjunto2)

# Interseccion
print(conjunto & conjunto2)

# Diferencia
print(conjunto - conjunto2)

# es posible hacer esto con funciones incluidas en la clase Set

#----------------------------------------------
