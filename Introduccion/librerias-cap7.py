import os # Importa todo lo contenido en la libreria x
import sys
import logging
from pathlib import Path # importa un modulo especifico
from time import sleep


logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(message)s'
)


def file_exists(file_name: str):
    return os.path.isfile(file_name)


def open_file(file_name: str):
    """ Abrir cualquier archivo """
    from subprocess import call
    if sys.platform == 'linux':
        call(["xdg-open", file_name])
    else:
        os.startfile(file_name)


def get_num() -> int:
    """ Octener valor de la primera linea
        de un archivo
    """
    from linecache import getline
    line_n = getline('contador.txt', 1)
    return int(line_n)


def encode_b64(word: str) -> str:
    """ Codificar en base64 """
    from base64 import b64encode
    encoded_word = b64encode(word.encode('utf-8'))
    return encoded_word.decode('utf-8')


def decode_b64(encoded_word: str) -> str:
    """ Decodificar y regresar al estado original """
    from base64 import b64decode
    decoded_word = b64decode(encoded_word.encode('utf-8'))
    return decoded_word.decode('utf-8')


def generate_password(plain_pass: str) -> str:
    """ Codificar una cadena en sha256 y devolver en Hex """
    from hashlib import sha256
    sha_signature = sha256(plain_pass.encode()).hexdigest()
    return sha_signature


# * Concatenar el nombre de un archivo a con la ruta
# * del archivo actual
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
file_rute = os.path.join(BASE_DIR, "contador.txt")

# * imprimir la plataforma de ejecucion
logging.debug("imprimiendo la plataforma actual")
print(sys.platform)

# * impime la ruta del archivo
logging.debug("imprimiendo la ruta absoluta del archivo")
print(file_rute)


# * gestionar de mejor manera una ruta
file = Path(file_rute)


# * imprime solo el nombre del archivo
# * excluyendo a la ruta
logging.debug("imprimiendo el nombre del archivo excluyendo la ruta")
print(file.name)


# * verificar que sea un archivo y que este exita
logging.debug("Verifica si es un archivo y si existe, devuelve un booleano")
print(file_exists(file_rute))
print(file_exists(BASE_DIR))

# * Transforma la notacion corta a una ruta
# * completa del usuario actual
logging.debug("Expande la ruta del archivo actual(~/)")
print(os.path.expanduser('~/Documentos/'))


# * codificar y decoficar en base64
logging.debug("Codifica y decodifica en base64")
encode_word = encode_b64("palabra en base 64")
print(encode_word)
decode_word = decode_b64(encode_word)
print(decode_word)

# * codificar una contraseña en base64
logging.debug("imprime una contraseña codificada en sha256")
print(generate_password('1234'))

# * ejecutando comandos del sistema operativo
logging.debug("ejecutando un comando del sistema operativo(ls)")
os.system('ls')

# * imprimir el numero octenido de archivo
logging.debug("imprime la primera linea de una archivo")
# print(get_num())
logging.debug("Abriendo un archivo en el programa predeterminado")
sleep(1)
open_file(file_rute)

logging.debug("eliminando el archivo de texto")
os.remove("contador.txt")
