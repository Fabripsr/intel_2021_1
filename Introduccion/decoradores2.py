def add_tag(tag: str):
    def inner_f(function):
        def wrapper(*args, **kwargs):
            tag_tem = function(*args, **kwargs)
            return f'<{tag}> {tag_tem} <\\{tag}>'
        return wrapper
    return inner_f


@add_tag("div")
def create_title(title: str, size: int = 1):
    return f'<h{size}> {title} <\\h{size}>'


def saludo():
    return "Hola programadores de python"


print(create_title("Funcion original", size=1))

saludo_html_tag = add_tag("li")(saludo)
print(saludo_html_tag())
print(saludo())
