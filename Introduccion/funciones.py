# Funciones
# deficion de una funcion
def some_function():  # una funcion se define anteponiendo la palabra reservada "def"
    # logica
    return "some return"   # toda funcion debe de retornar un valor!


print(some_function())


# funcion con argumentos
def function_with_args(some_arg):
    return some_arg


print(function_with_args("funcion con argumentos"))


# definicion de una funcion con argumentos por defecto
def function_with_args_default(some_default_argument="i'm default argument"):
    return f"Hi, {some_default_argument}"


print(function_with_args_default())
print(function_with_args_default("Curso de python"))


# funcion con argumentos de clave valor
def function_keyword(a, some_keyword="i'm keyword"):
    return f'{a} {some_keyword}'


print(function_keyword("Esto es una", some_keyword="funcion con keywords"))


# Argumentos arbitrarios
def function_arbitrary_args(*args):
    sum = 0
    for i in args:
        sum += i
    print(args)
    return sum


print(function_arbitrary_args(1, 2, 3, 4, 5, 6, 7, 8, 9))
numbers = (range(2, 10, 2))
print(function_arbitrary_args(*numbers))


# Argumentos clave valor arbitrarios
def function_arbitrary_kargs(**kwargs):
    for key, value in kwargs.items():
        print(f'{key}: {value}')
    return kwargs


print(function_arbitrary_kargs(arg1=1, arg2=2, arg3=3, arg4=4, arg5=5))

languages = {
    "python": "Slow",
    "c": "Super Fast",
    "rust": "Fast"
}
print(function_arbitrary_kargs(**languages))
