# Curso Intel_2021_1

Este repositorio contiene los ejemplos vistos en clase a si como los ejercicios
propuestos.

## Inicio
### Clonar el Repositorio
```` bash
git clone https://gitlab.com/R_Campos/intel_2021_1
````

### Actualizar el Repositorio
```` bash
git pull origin master
````

## Recursos

### Python

#### Libros
* Python Crash Course, Eric Matthes
* Effective Python, Brett Slatkin
* Fluent Python, Luciano Ramalho
* Advanced Guide to Python 3 Programming, John Hunt
* Mastering Python Design Patterns, Sakis Kasampalis
* Flask Web Development, Miguel Grinberg
* Functional Python Programming, Steven Lott
* Mastering Object-oriented Python, Steven Lott
* GUI Programming with Python: QT Edition, Boudewijn Rempt

#### Recursos Web
* [Aprender python](https://github.com/trekhleb/learn-python)
* [Python Basico](https://github.com/jerry-git/learn-python3)
* [Awesome python](https://github.com/vinta/awesome-python)
* [Algoritmos](https://github.com/TheAlgorithms/Python)
* [Python referencia](https://github.com/rasbt/python_reference)

### Linux

#### Libros
* Fundamentos de Administración de
Sistemas Linux, LFS
* Administración avanzada de GNU/Linux, Josep Jorba Esteve, Remo Suppi Boldrito
* Linux command line, Sander Van Vugt
* Pro Bash Programming, Chris F.A. Johnson
* Systems Programming in Unix/Linux, K. C. Wang
* Linux device drivers, Jonathan Corbet, Alessando Rubini, and Gre/home/cragg Kroah-Hartman

#### Recursos Web
* [Iniciantes en Linux](https://www.tecmint.com/free-online-linux-learning-guide-for-beginners/)
* [Enciclopedia para linux](https://www.linuxtopia.org/)

## Directrices para la evaluación de ejercicios prácticos

1. Cada ejercicio propuesto contará con un enunciado que describe el problema; el alumno deberá cumplir con el o los objetivos del ejercicio, tomando en cuenta todos los casos de uso.

2. Cada ejercicio debe guardarse dentro de una carpeta cuyo nombre será el nombre completo del alumno usando guiones bajos en vez de espacios.

3. El archivo Python debe llamarse como se indique en el enunciado del ejercicio.

4. Las funciones, métodos y clases tendrán un nombre predeterminado. Cada función o método debe retornar un valor(apegado a lo expuesto en el enunciado del ejercicio)

5. La evaluación de funcionamiento se hará mediante el uso de test unitarios, los test unitarios serán proporcionados por los profesores en una carpeta llamada “tests” la cual debe incluirse en la carpeta del ejercicio del alumno.

6. La solución de código debe apegarse a lo establecido en la [pep8](https://www.python.org/dev/peps/pep-0008/) y seguir las buenas prácticas de programación.

7. La solución de software debe ser consistente con el diagrama(clases, flujo) entregado.

8. En caso del uso de librerías externas a las de la biblioteca estándar incluir un archivo requirements.txt.
