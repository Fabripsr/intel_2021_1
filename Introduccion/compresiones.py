# lista
pairs = [(x, y) for x in range(2) for y in range(2)]
print(pairs)

# Conjuntos
s = {2 * x for x in range(10) if x ** 2 > 3}
print(s)

# Diccionarios
count_words = {s: len(s) for s in ["Python", "JavaScript", "Golang"]}
print(count_words)

reverse_count_words = {v: k for k, v in count_words.items()}
print(reverse_count_words)
