from functools import wraps


def function(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        print("Antes de la ejecución de la función a decorar")
        return f(*args, **kwargs)
    return wrapper


# Decorador con parametros
def function2(some_paramer):
    def inner_function(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            print(some_paramer)
            print("Antes de la ejecución de la función a decorar")
            return f(*args, **kwargs)
        return wrapper
    return inner_function


@function2("un parametro")
@function
def funcion_decorada_functools():
    """ Una Funcion decorada por dos decoradores """
    print("Los decoradores son hechos con wraps")


funcion_decorada_functools()

