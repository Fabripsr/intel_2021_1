
# cuenta del 0 al 9
for i in range(10): # si se pasa un solo parametro esto representa (0, stop, 1) inicio
# stop, step
    print(i)

# cuenta del 0 al 10 de 2 en 2
for i in range(0, 10, 2): # paremetros aceptados 3 (start, stop, step) o inicio, paro, pasos
    print(i)

# cuenta regresiva
for i in range(10, 0, -1):
    print(i)

# recorrido de estructuras

#lista

lista = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
lista2 = ["any", "null", "none"]

for i in lista:
    print(i)

for i in lista2:
    print(i)

# diccionarios

diccionarios = {
    "institucion": "inaoe",
    "curso": "python y linux",
    "participantes": "aspirantes intel"
}

# recorrer claves
for key in diccionarios:
    print(key)

# recorrer clave valor
for key, value in diccionarios.items():
    print(f'{key} -> {value}')

# recorrer solo valores
for value in diccionarios.values():
    print(value)

# sets

set_ejemplo = {1, 2, 3}

for s in set_ejemplo:
    print(s)

# tuplas

tupla = (10, "Git")

for i in tupla:
    print(i)
