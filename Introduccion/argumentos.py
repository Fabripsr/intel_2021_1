import logging

logging.basicConfig(level=logging.DEBUG, format="%(asctime)s - %(message)s")


def funcion_anotaciones(name: str) -> str:
    """Despues de los : se asigna el tipo
    esto no afecta la funcionalidad ni
    compromete a usar especificamente
    ese dato es solo una sugerencia.
    Tambien podemos definir el tipo
    de dato retorno despues ->
    """
    return f"hola {name}"


def ejemplo_argumentos_posicionales(a: int, b: int, c: int):
    print(a, b, c)


def ejemplo_argumentos_definidos(intel: str = "intel", python: str = "python"):
    """ Definimos argumentos que esperan un valor, por defecto ya tienen uno """
    print(intel, python)


def ejemplo_kwargs(**kwargs):
    """los ** indican la descompresion de
    diccionarios, entonces los **kwargs
    son un conjunto de argumentos clave valor
    """
    print(kwargs.get("intel"))
    print(kwargs.get("python"))


def ejemplo_args(*args):
    """el * indica descompresion de tupla
    entonces *args son elementos posisionales
    que recive la funcion
    """
    for i in args:
        print(i)


# pasamos un valor deacuerdo a la sugerencia de tipos
logging.debug("una funcion que contine anotaciones de tipo")
print(funcion_anotaciones("intel"))

# pasamos como parametros deacuerdo a los argumentos posisionales
ejemplo_argumentos_posicionales(1, 2, 3)

# posdemos pasar los argumentos clave valor en cualquier orden
logging.debug("funcion con argumentos definidos")
ejemplo_argumentos_definidos(python="mundo", intel="Hola")

# la funcion recibe como parametro n numero de argumentos clave valor
logging.debug("funcion con *kwargs")
ejemplo_kwargs(intel="la programacion", python="en python es genial")

# la funcion recibe como parametro n numero de argumentos posicionales
logging.debug("funcion con *args")
ejemplo_args(1, 2, 3)

# descompresion
numeros = (2, 4, 6)
temas = {"intel": "Esto es", "python": "una descompresion"}

# si descomprimimos una tupla esta puede pasarse como argumentos posicionales
logging.debug("funciones con tupla descompresa como parametros")
ejemplo_argumentos_posicionales(*numeros)
ejemplo_args(*numeros)

# si si descomprimimos un diccionario este toma la forma de argumentos clave valor
logging.debug("funciones con diccionario descompreso como parametros")
ejemplo_argumentos_definidos(**temas)
ejemplo_kwargs(**temas)

