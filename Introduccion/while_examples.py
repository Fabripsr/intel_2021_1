# Normal While
i = 1
while i < 5:
    print(i)
    i += 1

# --------- Declaraciones de control de bucle -----------------
# Rotura del bucle
i = 6
while i != 0:
   print(i)
   if i == 2:
       break
   i -= 1


# Rotura de un ciclo en el bucle
i = 10
while i > 0:
    i = i -1
    if i == 5:
        continue
    print ('Valor actual :', i)
print ("Final del Bucle")

# While-else
# Se ejecuta la sentencia else
i = 0
while i < 4:
    i += 1
    print(i)
else:
    print("No Break\n")

# No se ejecuta la sentencia else
i = 0
while i < 4:
    i += 1
    print(i)
    break
else:
    print("No Break")
