# Cambiar os.path por Path
import os
from pathlib import Path

# Mostrar directorio actual
print(os.getcwd())
print(Path.cwd())

# Verifique el directorio o archivo existente
print(os.path.exists('sample_data/README.md'))
print(Path('sample_data/README.md').exists())

# Crea un directorio
os.mkdir('test_dir')
Path('test_dir').mkdir()
# * Se recomienda siempe verificar la existencia
# con os.path
if not os.path.exists('test_dir'):
    os.mkdir('test_dir')
# en una sola linea de pathlib
Path('test_dir').mkdir(exist_ok=True)

# Profundidad multinivel
os.makedirs(os.path.join('test_dir', 'level_1a', 'level_2a', 'level_3a'))
Path(os.path.join('test_dir', 'level_1b', 'level_2b', 'level_3b')).mkdir(parents=True)

# Mostrar contenido del directorio
os.listdir('sample_data')
Path('sample_data').iterdir()
