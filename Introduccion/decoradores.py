def decorator(funcion):
    def wrapper():
        print('Antes de la ejecución de la función a decorar')
        funcion()
        print('Después de la ejecución de la función a decorar')
    return wrapper


def decorator2(some_parameter):
    def my_decorator(function):
        def wrapper(*args, **kwargs):
            print(some_parameter)
            return function(*args, **kwargs)
        return wrapper
    return my_decorator


@decorator2("un parametro")
@decorator
def funcion_decorada():
    """ Una Funcion decorada por dos decoradores """
    print("Los decoradores son echos de forma normal")


funcion_decorada()
