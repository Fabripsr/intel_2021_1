# Ejercicio de validación de contraseña

Crea una función que reciba como argumento un password y retorne True o False según sea
el caso.

Estructura del password:

* Tener 8 caracteres.
* Debe tener al menos dos caracteres iguales a ‘b’.
* No debe tener dos caracteres iguales a ‘k’.
* Debe tener un dígito: ‘\*’ o ‘+’ o ‘%’.

Nombre del archivo: contrasena.py

Nombre de la función: validar_contrasena

Leer las "directrices para la evaluación de ejercicios prácticos" en el README.md en la pagina principal del repositorio
