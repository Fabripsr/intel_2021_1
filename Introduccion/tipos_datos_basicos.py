#----------------Numeros---------------------
# Enteros
a = 1
b = 0x10
print(type(a), type(b))

# Flotante
c = 1.2
d = .5
g = .314e1
print(type(c), type(d), type(g))

# Complejos
e = 2+3j
f = complex(2, 3)
print(type(e), type(f))
print(f == e)

# Casting
# Entero/Cadena -> Flotante
print(float(4))
print(6 / 3)
print(float("3.14"))

# Entero/Cadena -> Integer
print(int(3.14))
print(int("3", base=10))
print(int("1011", base=2))
print(int("0b1011", base=0))
#---------------------------------------------

#-----------------Cadenas---------------------
string1 = ':cat:\n'
string2 = "Mustang 68"
string3 = """
Hola Estudiantes
"""
print(string1, string2, string3)
print("%s, %s, %s" % (string1, string2, string3))
print("{0}, {2}, {1}".format(string1, string2, string3))
print(f"{string1}, {string2}, {string3}")

# Concatenacion
concat = "hola " + "Coders"
concat_f_string = f"{b}:{a}"
print(concat)
print(concat_f_string)
print(len(concat))

# Casting
print(str(3.14))
print(str(3))
print(str([1, 2, 3]))
print(str((1, 2, 3)))
print(str({1, 2, 3}))
print(str({"python": '*.py', "rust": '*.rs', "c++": '*.cpp'}))

# Slicing
s = "hey soy un string"
print(s[8:])
print(s[4:7])
print(s[-9:])
print(s[::2])
print(s[2:11:3])
#---------------------------------------------

#----------------Byte-------------------------

byt = b'abc'
print(type(byt))
print(byt[0] == 'a')
print(byt[0] == 97)
print(len(byt))
#----------------------------------------------

#---------------Boleanos-----------------------
verdadero = True
falso = False

print(type(verdadero), type(falso))
#----------------------------------------------

#-----------------Nulos------------------------
print(None)
print(None is None)
#----------------------------------------------

